---
title: "STA242Assignment2"
author: "Guannan Liang"
date: "April 19, 2015"
output: word_document
---

## 1 Create the begining r*c grid with rho*r*c element
```{r}
create_grid <- 
  #the goal is to generate two r*c grids 
  #one grid for red car, there are ( r/(r+c) )*rho*r*c 1s for red car
  #another one for blue car,there are ( c/(r+c) )*rho*r*c 1s for blue car
  #there are total rho*r*c elements for both red car and blue car
  #
  #INPUT: r rows number, c columns number, rho density ( 0 < rho < 1 ).
  #OUTPUT: two r*c matrixs
function( r, c, rho){  
    # the number of cars
  int = round( rho*r*c )
  s = sample( 1:( r*c ), int )
  
    # generate matrix for red car
  #int.red = round( ( r /( r+c ) )*rho*r*c )
  int.red = round( ( 1/2 )*rho*r*c )
  s.red = sample( s , int.red ) 
  m1 = rep( 0, r*c )
  m1[ s.red ] = 1
  m.red = matrix( m1, nrow = c, ncol = r )
  
    #generate matrix for blue car
  int.blue = int - int.red
  s.blue = s[ !s %in% s.red ]
  m2 = rep( 0, r*c )
  m2[ s.blue ] = 1
  m.blue = matrix( m2, nrow = c, ncol = r )
  
  return( list( m.red, m.blue ) )  
}

```

## 2 one vector( one row/ one column ) and the vectors related with the vector change
```{r}
f.vector <-
  #We already get three columns: jth column in the red car's matrix,
  # ( j + 1 )th column in the red car's matrix, 
  # ( j + 1 )th column in the blue car's matrix.
  #
  # Or we already get three rows: ith row in the blue car's matrix,
  # ( i + 1 )th row in the blue car's matrix
  # ( i + 1 )th row in the red car's matrix
  #
  #For both two situations, we can simply say we get three vectors:
  #vector1, vector2, vector3
  #be careful the order of vectors
  #
  #INPUT : vector1, vector2, vector3
  #OUTPUT: vector1, vector2
function( vector1, vector2, vector3 ){
  
  loc = f.loc( vector1, vector2, vector3 )
  
  vector1[ loc ] = 0
  vector2[ loc ] = 1
  
  return( list( vector1, vector2 ) )
}
```

```{r}
f.loc <-
  #INPUT vector1, vector2, vector3
  #OUTPUT logical vector indicate location in the vector1
  #       which should move
function( vector1, vector2, vector3 ){
    #we use logical value to see where are the cars
    #logical value TRUE shows there is a car
  l1 = as.logical( vector1 )
  l2 = as.logical( vector2 )
  l3 = as.logical( vector3 )
    
    #find the car in vector1 could move
    #if there is a car in vector1 & there is no car in the same location in vector2 
    # and vector3, this car could move
  loc = l1 & ( !l2 ) &( !l3 )
  return( loc )
}

```

## 3 After one step, matrix for red car
```{r}
f.matrix.red <-
  #Or INPUT: m1 =  red car's matrix
  #          m2 =  blue car's matrix 
  #   OUTPUT:  new matrix  for red car
function( m1, m2 ){
  
    i = nrow( m1 ) 
    loc_0 = f.loc( m1[ i, ], m1[ 1, ], m2[ 1, ])
    temp = f.vector( m1[ i, ], m1[ 1, ], m2[ 1, ])
    m1[ i, ] = temp[[ 1 ]]
    m1[ 1, ] = temp[[ 2 ]]
    
    for( i in ( nrow( m1 ) - 1):2 ){
        temp = f.vector( m1[ i, ], m1[ i+1, ], m2[ i+1, ])
        m1[ i, ] = temp[[ 1 ]]
        m1[ i+1, ] = temp[[ 2 ]]
    }
    
    i = 1
    temp = f.vector( m1[ i, ], m1[ i+1, ], m2[ i+1, ])
    temp[[ 1 ]][ loc_0 ] = 1
    m1[ i, ] = temp[[ 1 ]]
    temp[[ 2 ]][ loc_0 ] = 0
    m1[ i+1, ] = temp[[ 2 ]]
    
  return( m1 )
}

```

```{r}
f.matrix.blue <-
  #INPUT: m1 = blue car's matrix 
  #       m2 = red car's matrix 
  #OUTPUT: new matrix for blue car
  
function( m1, m2 ){
    j = 1 
    loc_0 = f.loc( m1[ , j ], m1[ , ncol( m1 ) ], m2[ , ncol( m1 ) ])
    temp = f.vector( m1[ , j ], m1[ , ncol( m1 ) ], m2[ , ncol( m1 ) ])
    m1[ , j ] = temp[[ 1 ]]
    m1[ , ncol( m1 ) ] = temp[[ 2 ]]
    
    for( j in 2:(ncol( m1 )-1) ){
        temp = f.vector( m1[ , j ], m1[ , j-1 ], m2[ , j-1 ])
        m1[ , j ] = temp[[ 1 ]]
        m1[ , j-1 ] = temp[[ 2 ]]
    }
    
    j = ncol( m1 )
    temp = f.vector( m1[ , j ], m1[ , j-1 ], m2[ , j-1 ])
    temp[[ 1 ]][ loc_0 ] = 1
    m1[ , j ] = temp[[ 1 ]]
    temp[[ 2 ]][ loc_0 ] = 0
    m1[ , j-1 ] = temp[[ 2 ]]
    
  return( m1 )
}
```

```{r}
f.image <-
  #INPUT: red car's matrix m1
  #       blue car's matrix m2
  #OUTPUT: image 
function( m1, m2){
  m2[ which( m2==1) ] = 2
  m = m1 + m2; 
  image( m , col = c( "white", "red", "blue"))
}

```


## test
```{r}
f <-
function( ){
res = create_grid( 100, 300, 0.3)
m1 = res[[ 1 ]]
m2 = res[[ 2 ]]
#f.image( m1, m2)

library( animation)
#saveGIF( 
 for( n  in 1:1000){
  m1 = f.matrix.red( m1, m2)
  m2 = f.matrix.blue( m2, m1)
  #f.image( m1, m2)
#}, interval = 0.1, nmax = 30, ani.width = 600,
#ani.height = 600)
}
}


Rprof("/tmp/Maak.prof")
system.time(
f( )
)
Rprof(NULL)
p = summaryRprof("/tmp/Maak.prof")
p$by.self

```

library( codetools)
findGlobals( f.matrix.red )
showTree( f.matrix.red )
